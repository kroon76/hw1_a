/*
 * Calc.cpp
 *
 *  Created on: Aug 30, 2019
 *      Author: Admin
 */

#include "HW1a.h"
#include <iostream>

using HW1a::Calc;
using std::cin;
using std::cout;
using std::endl;


void Calc::set_Value(int a, int b){

 num1= a;
 num2=b;


}
 void Calc::operation_Choice()
 {

     char x;
	 cout << "Please enter the operation to perform: +, -, *, or /" << endl;
	 cin >> x;

	 switch (x)
	 {

	 case '+':
		 cout << num1+num2;
		 break;

	 case '-':
		 cout << num1-num2;
		 break;

	 case '*':
		 cout << num1*num2;
		 break;

	 case '/':
		 double p;
		 if ( num2 == 0 ){

			 cout << "invalid choice, cannot divide by zero" << endl;
			 break;
		 }

		 else

		 {
		 p = static_cast<double>(num1)/num2;
			 cout << p;
		 break;
		 }

	 default:

		 cout << "invalid choice, program is exiting" << endl;
		 break;

	 }

 }


