/*
 * HW1a.h
 *
 *  Created on: Aug 29, 2019
 *      Author: Admin
 */

#ifndef HW1A_H_
#define HW1A_H_

namespace HW1a {

class Calc {

public:
	int num1;
	int num2;
	int result() {
		return num1 + num2;
	}
	void set_Value(int, int);
	void operation_Choice();

};

}

#endif /* HW1A_H_ */
